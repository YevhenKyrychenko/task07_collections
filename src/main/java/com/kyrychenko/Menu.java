package com.kyrychenko;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private static Map<Integer, MenuItems> menuItemsMap = new HashMap<>();
    static {
        menuItemsMap.put(0, MenuItems.EXIT);
        menuItemsMap.put(1, MenuItems.EDIT);
        menuItemsMap.put(2, MenuItems.PRINT);
        menuItemsMap.put(3, MenuItems.VIEW);
    }

    public static void main(String[] args) {
        int input;
        System.out.println("Menu");
        Scanner sc = new Scanner(System.in);
        System.out.println("/************/");
        do {
            input = sc.nextInt();
            MenuItems menuItems = menuItemsMap.get(input);
            switch (menuItems){
                case EXIT:
                    System.out.println("Exiting");
                    break;
                case EDIT:
                    System.out.println("Editing");
                    break;
                case PRINT:
                    System.out.println("Printing");
                    break;
                case VIEW:
                    System.out.println("Viewing");
                    break;
                default:
                    System.out.println("Wrong Input\nTry Again!");
            }

        }while(input != 0);
        sc.close();
    }

}
