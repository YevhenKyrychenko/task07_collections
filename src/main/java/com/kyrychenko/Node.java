package com.kyrychenko;

public class Node<T extends Comparable<T>> {
    private Node<T> leftNode;
    private Node<T> rightNode;
    private T value;

    public Node(T value) {
        leftNode = null;
        rightNode = null;
        this.value = value;
    }

    public void setLeftNode(Node<T> node){
        this.leftNode = node;
    }

    public void setRightNode(Node<T> node){
        this.rightNode = node;
    }

    public void setValue(T value){
        this.value = value;
    }

    public Node<T> getLeftNode() {
        return leftNode;
    }

    public Node<T> getRightNode() {
        return rightNode;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        String result = value + "";
        if (leftNode  != null) result = leftNode.toString() + "-" + result;
        if (rightNode != null) result = result + "-" + rightNode.toString();
        return result;
    }
}
