package com.kyrychenko;

public class BinaryTree<T extends Comparable<T>> {
    private Node rootNode;

    public void putValue(T value) {
        insertNode(new Node<T>(value));
    }

    public void insertNode(Node<T> newNode){
        if (rootNode == null) {
            rootNode = new Node<T>(newNode.getValue());
        } else {
            insertNode(rootNode, newNode);
        }
    }

    public boolean containsValue(T value){
        if (getNode(rootNode, value) == null) {
            return false;
        }
        return true;
    }

    public Node<T> getNode(Node<T> currentNode, T value){
        if (currentNode == null)
            return null;
        if (currentNode.getValue() == value)
            return currentNode;
        if (value.compareTo(currentNode.getValue()) < 0 )
            return getNode(currentNode.getLeftNode(), value);
        return getNode(currentNode.getRightNode(), value);
    }

    private void insertNode(Node<T> currentNode, Node<T> newNode) {
        if (newNode.getValue().compareTo(currentNode.getValue()) < 0 ) {
            if (currentNode.getLeftNode() == null)
                currentNode.setLeftNode(newNode);
            else
                insertNode(currentNode.getLeftNode(), newNode);
        }
        if (newNode.getValue().compareTo(currentNode.getValue()) > 0 ) {
            if (currentNode.getRightNode() == null)
                currentNode.setRightNode(newNode);
            else
                insertNode(currentNode.getRightNode(), newNode);
        }
    }


    @Override
    public String toString(){
        if (rootNode != null){
            return rootNode.toString();
        }
        return "";
    }


    public static void main(String[] args) {
        BinaryTree<Integer> myTree = new BinaryTree<>();
        myTree.putValue(10);
        myTree.putValue(11);
        myTree.putValue(9);
        myTree.putValue(20);
        myTree.putValue(15);
        System.out.println(myTree.toString());
        System.out.println(myTree.containsValue(10));
    }
}
